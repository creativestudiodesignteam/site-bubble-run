<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php')?>
<body>
<?php include('includes/top-assets.php')?>
<header class="container-fluid header">

    <div class="bubble-holder one">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder two">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder three">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="mouse-scroll"></div>
    <div class="overlay-video d-none d-md-block">
        <video autoplay muted loop playsinline poster="images/bg-video.jpg" class="video">
            <source src="video/video-alta.mp4" type="video/mp4">
        </video>
    </div>
    <div class="row">
        <div class="col-lg-7 offset-lg-1 col-xs-12">
            <div class="extra-lg-text mb-3">
                <span>Ainda tem alguma <span class="other-color-4">dúvida?</span></span><br>
                <span>entre em <span class="other-color-4">contato </span>conosco</span> 
            </div>

            <div class="normal-text text-left text-header">
                <p>Caso tenha alguma dúvida referente as corridas, patrocínio ou dúvidas de como ser um voluntário entre em contato com a nossa equipe responderemos o mais breve possível!</p>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid box-content about-section about-page">
    <div class="row">
        <div class="col-md-12">
            <div class="boxy primary-color">
                <div class="row">
                    <div class="col text-center">
                        <h1 class="title title-sponsors">ENTRE EM CONTATO CONOSCO<br>RESPONDEREMOS O MAIS BREVE POSSÍVEL</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <form class="form-volunteer mt-4">
                            <div class="form-row">
                                <div class="col-md-5 offset-lg-1">
                                    <div class="form-group m-2">
                                        <input type="text" class="form-control" placeholder="Qual é o seu nome?*">
                                    </div>
                                </div>

                                <div class="col-md-5 m-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="seu e-mail*">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-5 offset-lg-1">
                                    <div class="form-group m-2">
                                        <input type="text" class="form-control" placeholder="Telefone*">
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group m-2">
                                        <select class="form-control" >
                                            <option selected disabled>Selecione a cidade da corrida*</option>
                                            <option>São Paulo</option>
                                            <option>Rio de Janeiro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-10 offset-lg-1">
                                    <div class="form-group m-2">
                                        <input type="text" class="form-control" placeholder="qual o assunto*">
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-row">
                                <div class="col-md-10 offset-lg-1">
                                    <div class="form-group m-2">
                                        <textarea type="text" class="form-control" rows="6" placeholder="Assunto"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-10 offset-lg-1">
                                    <div class="form-group m-2">
                                        <button type="submit" class="btn-form-volunteer">Enviar</button>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid default-content">
    <div class="row">
        <div class="col">
            <div class="lg-text">
                <span>Já fazemos parte de </span><br>
                <span>40+ Cidades e 500,000 Participantes</span><br>
                <span class="other-color-4">venha fazer parte dessa família.</span></div>
            
                <div class="btn-holder">
                    <a href="patrocine.php" class="cr-btn primary">Quero ser um patrocinador</a>
                </div>
        </div>
    </div>
</div>

<?php include('includes/footer.php')?>
<?php include('includes/scripts.php')?>

</body>
</html>