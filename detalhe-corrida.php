<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php')?>
<body>
<?php include('includes/top-assets.php')?>
<header class="container-fluid header">

    <div class="bubble-holder one">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder two">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder three">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="mouse-scroll"></div>
    <div class="overlay-video d-none d-md-block">
        <video autoplay muted loop playsinline poster="images/bg-video.jpg" class="video">
            <source src="video/video-alta.mp4" type="video/mp4">
        </video>
    </div>
    <div class="row">
        <div class="col">
            <div class="extra-lg-text text-center mb-3">
                <span>19 de junho</span><br>
                <span>Rio de janeiro</span><br>
                <span class="other-color-4">Aterro Flamengo</span>
            </div>

            <div class="btn-holder">
                <a href="#." target="_BLANK" class="cr-btn primary light-btn">Inscreva-se</a>
            </div>
        </div>
    </>
</header>

<div class="container-fluid box-content about-section about-page">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy-aux c1-color">
                <div class="aux-padding-content">
                    <div class="row">
                        <div class="col">
                            <h1 class="title">INFORMAÇÕES<br>SOBRE O DIA DA<br><span class="other-color-3">CORRIDA</span></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                            <div class="normal-text-sections-dark text-left">
                                <p>É aqui que você encontrará todas as informações necessárias para o dia da corrida e os dias que antecedem a corrida.<strong>Planeje chegar de 60 a 90 minutos antes do início das suas ondas para ter tempo de sobra para estacionar, obter seus ganhos Bubble RUN ™, fazer check-in, se necessário, e ficar situado.</strong></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="boxy primary-color bg-blue-5">
                            <div class="row">
                                <div class="col">
                                    <h1 class="title">INFORMAÇÕES<br>SOBRE O DIA DA<br>CORRIDA</h1>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                                    <div class="normal-text-sections-light text-left text-light text-detail-run">
                                        <p>Aqui está o nosso<br>Super-guia para todas as<br>coisas borbulhantes:</p>
                                    </div>

                                    <div class="btn-holder">
                                        <a href="#." target="_BLANK" class="cr-btn primary light-btn">Ver guia</a>
                                    </div>

                                    <a href="contato.php" class="btn-contact-more mt-4 d-inline-block">Se isso não ajudar,<br>
                                    <strong>clique aqui para entrar em contato.</strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy primary-color">
                <div class="row">
                    <div class="col">
                        <h1 class="title">FAQ</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <h1 class="title">RETIRADA DE<br>PACOTES</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left text-light">
                            <p>* Se você não puder participar da retirada de pacotes no dia anterior à corrida, ela será reservada no dia da corrida mediante uma taxa. Há uma taxa de US $ 5,00 por pacote, por participante pago no Race Day, apenas em dinheiro. Para evitar pagar essa taxa, participe pessoalmente da retirada de pacotes no dia anterior ou envie a um amigo uma cópia do seu código de barras da corrida.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <h1 class="title">Estacionamento</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left text-light">
                            <p>Há uma taxa de estacionamento de US $ 10 para o evento que é tratado pelo local ou por eventos legais. Com milhares de sorrisos presentes no dia da corrida, faça as mudanças exatas e não reduza muito o tempo antes do início da corrida. Coloque o máximo de borbulhantes (legalmente) em seu carro para economizar dinheiro!</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <h1 class="title">CURSO</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left text-light">
                            <p>A corrida será realizada em POSTPONED. Publicaremos uma corrida do local do mapa aqui assim que estiver disponível. Volte novamente à medida que nos aproximamos do dia da corrida.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <h1 class="title">DIA DE CORRIDA</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left text-light">
                            <p>Por favor, seja pontual no seu calor.<br> Quando o calor começar, pequenos grupos serão liberados a cada 60 a 90 segundos. Seu horário de início físico é baseado no primeiro a chegar, primeiro a ser servido, portanto, naturalmente, você começará mais cedo se entrar na rampa de início mais cedo. Seu tempo de aquecimento será dentro da hora em que você foi designado.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid box-content volunteer-section">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy primary-color">
                <div class="row">
                    <div class="col">
                        <h1 class="title">QUER SER<br>UM <span class="other-color-4">VOLUNTÁRIO</span><br> NA BUBBLE RUN?</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left">
                            <p>Você respondeu 'sim' a alguma dessas perguntas? Nós pensamos que sim. Queremos que você seja voluntário para trabalhar com o  Bubble Run ™! Inscreva-se para uma cidade perto de você abaixo!</p>
                            <p>Voluntários com 16 anos ou mais. Se os voluntários tiverem menos de 18 anos, DEVEM ter um supervisor adulto (pai, professor, tutor ou diretor do clube). Não são permitidos voluntários com menos de 16 anos, por segurança e logística.</p>
                        </div>
                    </div>
                </div>

                <div class="btn-holder">
                    <a href="voluntario.php" class="cr-btn primary light-btn">Saiba mais sobre</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy img-box">
                <div class="img"><img src="images/img2.jpg" alt=""></div>
                <div class="bottom-text">
                    <div class="link">QUER SABER MAIS?</div>
                    <div class="text">Clique aqui e descubra mais</div>
                </div>
                <a href="voluntario.php" class="project-link-full"></a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid default-content">
    <div class="row">
        <div class="col">
            <div class="lg-text">
                <span>Já fazemos parte de </span><br>
                <span>40+ Cidades e 500,000 Participantes</span><br>
                <span class="other-color-4">venha fazer parte dessa família.</span></div>
            
                <div class="btn-holder">
                    <a href="patrocine.php" class="cr-btn primary">Quero ser um patrocinador</a>
                </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php')?>
<?php include('includes/scripts.php')?>

</body>
</html>