<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php')?>
<body>
<?php include('includes/top-assets.php')?>
<header class="container-fluid header">

    <div class="bubble-holder one">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder two">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder three">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="mouse-scroll"></div>
    <div class="overlay-video d-none d-md-block">
        <video autoplay muted loop playsinline poster="images/bg-video.jpg" class="video">
            <source src="video/video-alta.mp4" type="video/mp4">
        </video>
    </div>
    <div class="row">
        <div class="col-lg-6 offset-lg-1 col-xs-12">
            <div class="extra-lg-text mb-3">
                <span>Conheça um pouco</span><br>
                <span>mais sobre a corrida 5k</span><br>
                <span>a <span class="other-color-4">bubble Run</span></span>
            </div>

            <div class="normal-text text-left text-header">
                <p>Temos ótimas notícias para você! O Bubble Run trouxe aqueles bons e velhos dias de volta e a melhor parte é que você não precisa limpar a bagunça. </p>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid box-content about-section about-page">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy-aux c1-color">
                <div class="aux-padding-content">
                    <div class="row">
                        <div class="col">
                            <h1 class="title">VOCÊ JÁ<br>CONHECE A<br><span class="other-color-3">BUBBLE RUN</span></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                            <div class="normal-text-sections-dark text-left">
                                <p>Todos nos lembramos dos bons e velhos dias em que as bolhas podem ser o destaque de sua semana. Lembra quando soprar bolhas no quintal era uma razão para voltar da escola para casa? Que tal ensaboar seu carro na garagem para uma boa lavagem? Ou você se lembra quando não havia nada melhor do que um banho de espuma? (Ok, isso ainda é verdade hoje!) </p>
                                <p>Bem, temos ótimas notícias para você! O Bubble Run trouxe aqueles bons e velhos dias de volta - e a melhor parte é que você não precisa limpar a bagunça. Frequentemente imitado, nunca duplicado, o Bubble Run é o original da bolha. Você pode esperar cinco quilômetros (3,1 milhas) de um momento borbulhante e divertido enquanto corre, caminha, dança ou até empurra um carrinho de bebê através de nossa divertida corrida. </p>
                            </div>
                        </div>
                    </div>
    
                    <div class="btn-holder">
                        <a href="voluntario.php" class="cr-btn primary">seja voluntário</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="boxy img-box">
                            <div class="img"><img src="images/img3.jpg" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy primary-color">
                <div class="row">
                    <div class="col">
                        <h1 class="title">PRA QUEM<br>É ISSO?</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left text-light">
                            <p>Você é um atleta competitivo? Uma família procurando algo emocionante para fazer juntos? Ou você está apenas olhando para sair do sofá e se mexer? Se você respondeu sim a alguma dessas perguntas, então o Bubble Run é para você. O Bubble Run é um percurso plano para qualquer idade ou nível de condicionamento físico. Se você pode passar uma hora caminhando e conversando com familiares ou amigos, está pronto para o Bubble Run. Congratulamo-nos com todos e encorajamos você a se mover no seu próprio ritmo. Realmente não há pressão.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <h1 class="title">A BUBBLE RUN É<br>APENAS MAIS UM 5K?</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left text-light">
                            <p>De jeito nenhum! Bubble Run começa com um estrondo. Você será recebido por canhões gigantes de espuma que o cobrem com espuma enquanto um DJ toca música divertida em nossa pré-festa. Então, na hora de correr, você experimentará quatro pântanos de espuma colorida (vermelho, verde, azul e amarelo.) Então, o que acontece quando você termina? Você adivinhou - ainda mais bolhas! Na nossa festa depois (que é como um mini-festival de música), esses canhões de espuma serão transformados a todo vapor.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <h1 class="title">VOCÊ DEVE<br>EXECUTAR UMA <br>BOLHA?</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left text-light">
                            <p>Você não precisa acreditar em nossa palavra (apesar de dizer que sim.) Desde 2013, o Bubble Run já recebeu mais de 1,2 milhão de participantes em todo o país.  Dê uma rápida olhada nas mídias sociais e você descobrirá quilômetros e quilômetros de sorrisos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid default-content">
    <div class="row">
        <div class="col">
            <div class="lg-text">
                <span>Já fazemos parte de </span><br>
                <span>40+ Cidades e 500,000 Participantes</span><br>
                <span class="other-color-4">venha fazer parte dessa família.</span></div>
            
                <div class="btn-holder">
                    <a href="patrocine.php" class="cr-btn primary">Quero ser um patrocinador</a>
                </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php')?>
<?php include('includes/scripts.php')?>

</body>
</html>