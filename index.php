<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php')?>
<body>
<?php include('includes/top-assets.php')?>
<header class="container-fluid header">

    <div class="bubble-holder one">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder two">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder three">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="mouse-scroll"></div>
    <div class="overlay-video d-none d-md-block">
        <video autoplay muted loop playsinline poster="images/bg-video.jpg" class="video">
            <source src="video/video-alta.mp4" type="video/mp4">
        </video>
    </div>
    <div class="row">
        <div class="col-lg-6 offset-lg-1 col-xs-12">
            <div class="extra-lg-text mb-3">
                <span>uma corrida</span><br>
                <span>divertida e cheia</span><br>
                <span>de espuma, conheça</span><br>
                <span>a <span class="other-color-4">bubble Run</span></span>
            </div>

            <div class="normal-text text-left text-header">
                <p>Temos ótimas notícias para você! O Bubble Run trouxe aqueles bons e velhos dias de volta e a melhor parte é que você não precisa limpar a bagunça. </p>
            </div>
        </div>

        <div class="col-lg-3 cities">
            <div class="content-cities text-left">
                <div class="title-cities text-left mb-3">
                    <img src="images/icon-cities.png" class="icon-cities" alt="icone próximas corridas" />
                    <h2>Confira as datas para as <span class="other-color-3">Corridas</span></h2>
                </div>
                <a href="detalhe-corrida.php" class="link-cities">
                    <div class="box-run-citie">
                        <div class="place-text text-left">
                            <div class="data mb-3">
                                <span>16.06</span><br>
                                <span>Junho</span><br>
                            </div>
    
                            <span class="state">Rio de Janeiro</span>
                            <span class="address">Aterro do flamengo</span>
                        </div>
                    </div>
                </a>

                <a href="detalhe-corrida.php" class="link-cities d-none d-md-block">
                    <div class="box-run-citie">
                        <div class="place-text text-left">
                            <div class="data mb-3">
                                <span>20.06</span><br>
                                <span>Junho</span><br>
                            </div>
    
                            <span class="state">São Paulo</span>
                            <span class="address">parque Villa Lobos</span>
                        </div>
                    </div>
                </a>
                <a href="cidades.php" class="btn-cities">Ver todas as datas <img src="images/arrow.png" alt=""></a>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid box-content about-section">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy c1-color">
                <div class="row">
                    <div class="col">
                        <h1 class="title">VOCÊ JÁ<br>CONHECE A<br><span class="other-color-3">BUBBLE RUN</span></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-dark text-left">
                            <p>Todos nos lembramos dos bons e velhos dias em que as bolhas podem ser o destaque de sua semana. Lembra quando soprar bolhas no quintal era uma razão para voltar da escola para casa? Que tal ensaboar seu carro na garagem para uma boa lavagem? Ou você se lembra quando não havia nada melhor do que um banho de espuma? (Ok, isso ainda é verdade hoje!) </p>
                        </div>
                    </div>
                </div>

                <div class="btn-holder">
                    <a href="sobre.php" class="cr-btn primary">Saiba mais sobre</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy img-box">
                <div class="img"><img src="images/img1.jpg" alt=""></div>
                <div class="bottom-text">
                    <div class="link">QUER SABER MAIS?</div>
                    <div class="text">Clique aqui e descubra mais</div>
                </div>
                <a href="sobre.php" class="project-link-full"></a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid box-content volunteer-section">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy primary-color">
                <div class="row">
                    <div class="col">
                        <h1 class="title">QUER SER<br>UM <span class="other-color-4">VOLUNTÁRIO</span><br> NA BUBBLE RUN?</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left">
                            <p>Você respondeu 'sim' a alguma dessas perguntas? Nós pensamos que sim. Queremos que você seja voluntário para trabalhar com o  Bubble Run ™! Inscreva-se para uma cidade perto de você abaixo!</p>
                            <p>Voluntários com 16 anos ou mais. Se os voluntários tiverem menos de 18 anos, DEVEM ter um supervisor adulto (pai, professor, tutor ou diretor do clube). Não são permitidos voluntários com menos de 16 anos, por segurança e logística.</p>
                        </div>
                    </div>
                </div>

                <div class="btn-holder">
                    <a href="voluntario.php" class="cr-btn primary light-btn">Saiba mais sobre</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy img-box">
                <div class="img"><img src="images/img2.jpg" alt=""></div>
                <div class="bottom-text">
                    <div class="link">QUER SABER MAIS?</div>
                    <div class="text">Clique aqui e descubra mais</div>
                </div>
                <a href="voluntario.php" class="project-link-full"></a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid default-content">
    <div class="row">
        <div class="col">
            <div class="lg-text">
                <span>Já fazemos parte de </span><br>
                <span>40+ Cidades e 500,000 Participantes</span><br>
                <span class="other-color-4">venha fazer parte dessa família.</span></div>
            
                <div class="btn-holder">
                    <a href="patrocine.php" class="cr-btn primary">Quero ser um patrocinador</a>
                </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php')?>
<?php include('includes/scripts.php')?>

</body>
</html>