<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php')?>
<body>
<?php include('includes/top-assets.php')?>
<header class="container-fluid header">

    <div class="bubble-holder one">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder two">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder three">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="mouse-scroll"></div>
    <div class="overlay-video d-none d-md-block">
        <video autoplay muted loop playsinline poster="images/bg-video.jpg" class="video">
            <source src="video/video-alta.mp4" type="video/mp4">
        </video>
    </div>
    <div class="row">
        <div class="col-lg-6 offset-lg-1 col-xs-12">
            <div class="extra-lg-text mb-3">
                <span>Seja um patrocinador</span><br>
                <span>oficial da <span class="other-color-4">bubble run</span></span>
            </div>

            <div class="normal-text text-left text-header">
                <p>Temos ótimas notícias para você! O Bubble Run trouxe aqueles bons e velhos dias de volta e a melhor parte é que você não precisa limpar a bagunça. </p></p>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid box-content about-section">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy c1-color box-cities-run">
                <div class="row">
                    <div class="col">
                        <h1 class="title">BUBBLE RUN<br><span class="other-color-3">PATROCINADOR</span></h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left">
                            <p>Exiba seu produto para milhares de participantes do Bubble RUN ™!</p>
                            <p>40+ cidades. 500.000 participantes.</p>
                            <p>Torne-se um patrocinador / fornecedor conosco e participe dos eventos 5K mais limpos e divertidos do país! Se você é uma empresa nacional que procura marketing criativo ou uma empresa local que tenta aumentar a exposição da sua marca, o Bubble RUN é para você. Em cada evento, você será exposto a uma média de 10.000 participantes entusiasmados.</p>
                            <p>Não espere Torne o marketing da sua empresa "pop" neste evento emocionante e de rápido crescimento. Obtenha mais informações preenchendo o formulário abaixo.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy img-box">
                <div class="img"><img src="images/img6.jpg" alt=""></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid box-content about-section about-page">
    <div class="row">
        <div class="col-md-12">
            <div class="boxy primary-color">
                <div class="row">
                    <div class="col text-center">
                        <h1 class="title title-sponsors">PARA SER UM PATROCINADOR OFICIAL<br>DA BUBBLE RUN PREENCHA OS CAMPOS ABIXO</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <form class="form-volunteer mt-4">
                            <div class="form-row">
                                <div class="col-md-5 offset-lg-1">
                                    <div class="form-group m-2">
                                        <input type="text" class="form-control" placeholder="Qual é o seu nome?*">
                                    </div>
                                </div>

                                <div class="col-md-5 m-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="seu e-mail*">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-5 offset-lg-1">
                                    <div class="form-group m-2">
                                        <input type="text" class="form-control" placeholder="Telefone*">
                                    </div>
                                </div>

                                <div class="col-md-5 m-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Nome do patrocinador*">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-5 offset-lg-1">
                                    <div class="form-group m-2">
                                        <input type="text" class="form-control" placeholder="Endereço*">
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group m-2">
                                        <select class="form-control" >
                                            <option selected disabled>Selecione a cidade da corrida*</option>
                                            <option>São Paulo</option>
                                            <option>Rio de Janeiro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-5 offset-lg-1">
                                    <div class="form-group m-2">
                                        <select class="form-control">
                                            <option selected disabled>Selecione o pais*</option>
                                            <option>Brasil</option>
                                            <option>Estados Unidos</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group m-2">
                                        <input type="text" class="form-control" placeholder="data de nasciemnto*">
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-row">
                                <div class="col-md-10 offset-lg-1">
                                    <div class="form-group m-2">
                                        <textarea type="text" class="form-control" rows="6" placeholder="Descrição de produtos e / ou serviços *"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-10 offset-lg-1">
                                    <div class="form-group m-2">
                                        <textarea type="text" class="form-control" rows="6" placeholder="Objetivos da parceria"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-10 offset-lg-1">
                                    <div class="form-group m-2">
                                        <button type="submit" class="btn-form-volunteer">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('includes/footer.php')?>
<?php include('includes/scripts.php')?>

</body>
</html>