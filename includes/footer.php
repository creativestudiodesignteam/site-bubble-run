<footer class="container-fluid footer">
    <div class="row">
        <div class="col">
            <div class="lg-text">
                <span>Quem participa não se arrepende</span><br>
                <span>qualquer dúvida entre em contato</span>
            </div>
            <div class="normal-text">
                <p>We’ll take your business to the next level, with our proven<br>strategies, latest technologies and friendly creatives that<br>will work to produce the best outcome possible.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="contact-info-holder">
                <div class="title">Contato</div>
                <div class="contact-info">+55 11 95959-2559</div>
            </div>
        </div>
        <div class="col">
            <div class="contact-info-holder">
                <div class="title">E-mail</div>
                <div class="contact-info"><a href="mailto:contato@bubblerun.com.br">contato@bubblerun.com.br</a></div>
                <div class="social-media">
                    <div class="social-link-holder"><a href="#">Facebook</a></div>
                    <div class="social-link-holder"><a href="#">Instagram</a></div>
                    <div class="social-link-holder"><a href="#">Youtube</a></div>
                    <div class="social-link-holder"><a href="#">Twitter</a></div>
                </div>
            </div>
        </div>
    </div>
</footer>