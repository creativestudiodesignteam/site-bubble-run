<div class="menu-toggle">
    <div class="icon"></div>
</div>
<div class="main-menu">
    <div class="contant-info">
        <div><a href="mailto:hello@cre8.com">contato@bubblerun.com.br</a></div>
        <div>+55 11 95959-2559</div>
    </div>
    <div class="menu-links">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="sobre.php">Sobre</a></li>
            <li><a href="cidades.php">Cidades</a></li>
            <li><a href="voluntario.php">Voluntário</a></li>
            <li><a href="patrocine.php">Patrocine</a></li>
            <li><a href="contato.php">Contato</a></li>
        </ul>
    </div>
    <div class="social-media">
        <div class="social-link-holder"><a href="#">Facebook</a></div>
        <div class="social-link-holder"><a href="#">Instagram</a></div>
        <div class="social-link-holder"><a href="#">Youtube</a></div>
        <div class="social-link-holder"><a href="#">Twitter</a></div>
    </div>
</div>
<nav class="container-fluid cnav">
    <div class="row">
        <div class="col">
            <div class="logo-holder">
                <a href="index.php"><img class="logo" src="images/logo.svg" alt="CRE8"></a>
            </div>
        </div>
        <div class="col text-right">
            <div class="social-media">
                <div class="social-link-holder"><a href="#">Facebook</a></div>
                <div class="social-link-holder"><a href="#">Instagram</a></div>
                <div class="social-link-holder"><a href="#">Youtube</a></div>
                <div class="social-link-holder"><a href="#">Twitter</a></div>
            </div>
        </div>
    </div>
</nav>