<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php')?>
<body>
<?php include('includes/top-assets.php')?>
<header class="container-fluid header">

    <div class="bubble-holder one">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder two">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder three">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="mouse-scroll"></div>
    <div class="overlay-video d-none d-md-block">
        <video autoplay muted loop playsinline poster="images/bg-video.jpg" class="video">
            <source src="video/video-alta.mp4" type="video/mp4">
        </video>
    </div>
    <div class="row">
        <div class="col-lg-6 offset-lg-1 col-xs-12">
            <div class="extra-lg-text mb-3">
                <span>Você quer fazer</span><br>
                <span class="other-color-4">parte da magia?</span><br>
                <span>e ser um voluntário</span><br>
            </div>

            <div class="normal-text text-left text-header">
                <p>Queremos que você seja voluntário para trabalhar com o  Bubble Run ™! Inscreva-se para uma cidade perto de você abaixo!</p>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid box-content about-section about-page">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy-aux c1-color">
                <div class="aux-padding-content">
                    <div class="row">
                        <div class="col">
                            <h1 class="title">VOCÊ QUER FAZER<br><span class="other-color-3">PARTE DA MAGIA?</span></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                            <div class="normal-text-sections-dark text-left">
                                <p><strong>Deseja fazer parte da experiência do Bubble Run e, ao mesmo tempo, torná-lo melhor para os participantes?</strong></p>
                                <p><strong>Deseja dedicar seu tempo e ajudar a retribuir instituições de caridade locais e o movimento CoolEvents?</strong></p>
                                <p><strong>Deseja receber até US $ 25 em mercadorias de corrida grátis OU uma * entrada de corrida gratuita quando concluir seu turno INTEIRO?</strong></p>
                                <p><i>Nota : * A entrada gratuita no evento é limitada apenas à criação de voluntários diurnos e voluntários de coleta pré-pacote de sexta-feira. Exige que você se inscreva no evento como participante, pague, assine a isenção de participante e complete todo o turno de voluntário. Após o evento, envie um e-mail para o atendimento ao cliente em  info @ bubblerun.com  para solicitar seu reembolso de entrada para o voluntariado.</i></p>
                                <p><i>Nota : Se você escolher o crédito da mercadoria, ele será resgatado na loja de mercadorias no local após a conclusão de todo o turno e a entrada e saída.</i></p>
                                <p><strong>Você deve entrar e sair do seu turno de voluntário para ser confirmado e receber crédito. </strong></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="boxy primary-color bg-blue-5">
                            <div class="row">
                                <div class="col">
                                    <p class="title-text text-light">Você respondeu 'sim' a alguma dessas perguntas? Nós pensamos que sim. Queremos que você seja voluntário para trabalhar com o  Bubble Run ™! Inscreva-se para uma cidade perto de você abaixo!</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                                    <div class="normal-text-sections-light text-left text-light">
                                        <p>Congratulamo-nos com voluntários com 16 anos ou mais. Se os voluntários tiverem menos de 18 anos, DEVEM ter um supervisor adulto (pai, professor, tutor ou diretor do clube). Não são permitidos voluntários com menos de 16 anos, por segurança e logística. Todos os voluntários devem assinar uma renúncia voluntária, aqueles com menos de 18 anos de idade também precisarão assinar a assinatura pelos pais. * Se você for voluntário para concluir o horário de serviço comunitário para a escola ou o trabalho, solicitamos que você traga um formulário que deve ser assinado pelo supervisor no momento em que for voluntário.</p>
                                        <p>Se você conhece um grupo de voluntários que deseja nos ajudar em um de nossos eventos, envie um email para  voluntários@coolevents.com .</p>
                                        <p>Te vejo em breve!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy primary-color">
                <div class="row">
                    <div class="col">
                        <h1 class="title">PREENCHA OS<br>CAMPOS ABAIXO<br>PARA SE CADASTRAR</h1>
                        <p class="text-light">Queremos que você seja voluntário para trabalhar com o  Bubble Run ™! Inscreva-se para uma cidade perto de você abaixo!</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <form class="form-volunteer mt-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Qual é o seu nome?*">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="seu e-mail*">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Telefone*">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Informe seu CPF*">
                            </div>

                            <div class="form-group">
                                <input type="data" class="form-control" placeholder="Data de nascimento*">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Seu endereço*">
                            </div>

                            <div class="form-group">
                                <select class="form-control" >
                                    <option selected disabled>Selecione a cidade da corrida*</option>
                                    <option>São Paulo</option>
                                    <option>Rio de Janeiro</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <textarea type="text" class="form-control" rows="6" placeholder="Alguma Observação?"></textarea>
                            </div>
                            <p class="text-light text-center"><i>Campos com (*) são de preenchimento obrigatório!</i></p>
                            <button type="submit" class="btn-form-volunteer">Cadastrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid default-content">
    <div class="row">
        <div class="col">
            <div class="lg-text">
                <span>Já fazemos parte de </span><br>
                <span>40+ Cidades e 500,000 Participantes</span><br>
                <span class="other-color-4">venha fazer parte dessa família.</span></div>
            
                <div class="btn-holder">
                    <a href="patrocine.php" class="cr-btn primary">Quero ser um patrocinador</a>
                </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php')?>
<?php include('includes/scripts.php')?>

</body>
</html>