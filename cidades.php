<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php')?>
<body>
<?php include('includes/top-assets.php')?>
<header class="container-fluid header">

    <div class="bubble-holder one">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder two">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="bubble-holder three">
        <div class="bubble-rotate-offset">
            <figure class="ball bubble"></figure>
        </div>
    </div>

    <div class="mouse-scroll"></div>
    <div class="overlay-video d-none d-md-block">
        <video autoplay muted loop playsinline poster="images/bg-video.jpg" class="video">
            <source src="video/video-alta.mp4" type="video/mp4">
        </video>
    </div>
    <div class="row">
        <div class="col-lg-6 offset-lg-1 col-xs-12">
            <div class="extra-lg-text mb-3">
                <span>Fique por</span><br>
                <span>dentro das cidades</span><br>
                <span>a <span class="other-color-4">Participantes</span></span>
            </div>

            <div class="normal-text text-left text-header">
                <p>Ver todas as cidades participanetes.
                Devido o COVID-19 as datas podem ser alteradas sem aviso prévio qualquer dúvida entre em contato com nossa equipe</p>
            </div>
        </div>

        <div class="col-lg-3 cities">
            <div class="content-cities text-left">
                <div class="title-cities text-left mb-3">
                    <img src="images/icon-cities.png" class="icon-cities" alt="icone próximas corridas" />
                    <h2>Confira as datas para as <span class="other-color-3">Corridas</span></h2>
                </div>
                <a href="detalhe-corrida.php" class="link-cities">
                    <div class="box-run-citie">
                        <div class="place-text text-left">
                            <div class="data mb-3">
                                <span>16.06</span><br>
                                <span>Junho</span><br>
                            </div>
    
                            <span class="state">Rio de Janeiro</span>
                            <span class="address">Aterro do flamengo</span>
                        </div>
                    </div>
                </a>

                <a href="detalhe-corrida.php" class="link-cities d-none d-md-block">
                    <div class="box-run-citie">
                        <div class="place-text text-left">
                            <div class="data mb-3">
                                <span>20.06</span><br>
                                <span>Junho</span><br>
                            </div>
    
                            <span class="state">São Paulo</span>
                            <span class="address">parque Villa Lobos</span>
                        </div>
                    </div>
                </a>
                <a href="cidades.php" class="btn-cities">Ver todas as datas <img src="images/arrow.png" alt=""></a>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid box-content about-section">
    <div class="row">
        <div class="col-md-6">
            <div class="boxy c1-color box-cities-run">
                <div class="row">
                    <div class="col">
                        <h1 class="title">19.06<br>JUNHO<br>RIO DE JANEIRO</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light locale-cities-run text-left">
                            <span class="other-color-3">ATERRO DO FLAMENGO</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left">
                            <p>O registro inclui: medalha do Bubble Run Finishers, camiseta do Bubble RUN, tatuagem temporária, número da jardineira da corrida e entrada na mais radiante celebração do Bubble 5K depois!</p>
                        </div>
                    </div>
                </div>

                <div class="btn-holder">
                    <a href="#." target="_BLANK" class="cr-btn primary">Inscreva-se</a>
                </div>

                <div class="btn-holder">
                    <a href="detalhe-corrida.php" class="cr-btn primary">Saiba mais</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy img-box">
                <div class="img"><img src="images/img4.jpg" alt=""></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid box-content volunteer-section">
    <div class="row">
        <div class="col-md-6 d-none d-md-block">
            <div class="boxy img-box">
                <div class="img"><img src="images/img5.jpg" alt=""></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="boxy primary-color box-cities-run">
                <div class="row">
                    <div class="col">
                        <h1 class="title">20.06<br>JUNHO<br>SÃO PAULO</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left locale-cities-run">
                            <span class="other-color-3 text-light">PARQUE VILLA LOBOS</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-12 col-lg-10">
                        <div class="normal-text-sections-light text-left">
                            <p>O registro inclui: medalha do Bubble Run Finishers, camiseta do Bubble RUN, tatuagem temporária, número da jardineira da corrida e entrada na mais radiante celebração do Bubble 5K depois!</p>
                        </div>
                    </div>
                </div>

                <div class="btn-holder">
                    <a href="#." target="_BLANK" class="cr-btn primary light-btn">Inscreva-se</a>
                </div>

                <div class="btn-holder">
                    <a href="detalhe-corrida.php" class="cr-btn primary light-btn">Saiba mais</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid default-content">
    <div class="row">
        <div class="col">
            <div class="lg-text">
                <span>Já fazemos parte de </span><br>
                <span>40+ Cidades e 500,000 Participantes</span><br>
                <span class="other-color-4">venha fazer parte dessa família.</span></div>
            
                <div class="btn-holder">
                    <a href="patrocine.php" class="cr-btn primary">Quero ser um patrocinador</a>
                </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php')?>
<?php include('includes/scripts.php')?>

</body>
</html>